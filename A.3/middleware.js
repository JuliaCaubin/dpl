const express = require('express');
const logger = require('morgan');
const app = express();
app.use(logger('dev'));
const port = 3000;

app.get('/about', function(req, res){
    res.send('Nombre: Julia F. Caubín Alonso. \n Edad: 22 años. \n Comida favorita: Pollo al curry');
});

app.listen(port, () => {
    console.log('Example app listening on port ${port}!')
  });
var http = require('http');
var puerto = 3000;

http.createServer(function (req, res) {
    var hora = Date();
    //console.log('La hora es '+" "+hora);
    var print = hora.toString();
    res.writeHead(200, { 'Content-Type': 'text/html' });
    res.end(print);
}).listen(puerto);

console.log("Servidor corriendo en el puerto 3000");

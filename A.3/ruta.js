const express = require('express');
const app = express();
const port = 3000;

app.get('/about',function(req, res) {
    res.send('Nombre: Julia F. Caubín Alonso. \n Edad: 22 años. \n Comida favorita: Pollo al curry');
});

app.listen(port, () => {
    console.log('Example app listening on port ${port}!')
  });